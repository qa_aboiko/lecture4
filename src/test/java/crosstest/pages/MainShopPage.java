package crosstest.pages;

import crosstest.model.ProductData;
import crosstest.utils.Properties;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.events.EventFiringWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import java.util.List;


public class MainShopPage {
    private EventFiringWebDriver driver;
    private By allProductsButton = By.className("all-product-link");
    private By productItemName = By.className("product-title");
    private By nextPageButton = By.className("next");
    private By productViewButton = By.cssSelector("span[itemprop=\"price\"]");
    private By numberOfProductsOnPage = By.xpath("//section/div/div/div/p");
    private By productName = By.cssSelector("h1[itemprop=\"name\"]");
    private By productPrice = By.cssSelector("span[itemprop=\"price\"]");
    private By productQuantity = By.cssSelector("div[class=\"product-quantities\"] > span");



    public MainShopPage (EventFiringWebDriver driver) {this.driver = driver;}

    public void open() {driver.get(Properties.getBaseUrl());}

    public void productDisplayVerification(ProductData myNewProduct) {

        WebDriverWait wait = new WebDriverWait(driver, 10);
        wait.until(ExpectedConditions.elementToBeClickable(allProductsButton));
        driver.findElement(allProductsButton).click();
        int amountProductsOnPage = Integer.parseInt(driver.findElement(numberOfProductsOnPage).getText().replaceAll("[^0-9]", ""));;
        final int pageSize = 12;

        for(int i = 0; i < amountProductsOnPage; i++){

            amountProductsOnPage = Integer.parseInt(driver.findElement(numberOfProductsOnPage).getText().replaceAll("[^0-9]", ""));
            By lastProductOnPage = By.xpath("//div/article" + "[" + amountProductsOnPage + "]");
            wait.until(ExpectedConditions.presenceOfElementLocated(lastProductOnPage));
            List<WebElement> productItemNames = driver.findElements(productItemName);
            if(productItemNames.get(i).getText().equals(myNewProduct.getName())) {
                //System.out.println(myNewProduct.getName() + " == " + productItemNames.get(i).getText() );
                Assert.assertEquals((productItemNames.get(i).getText()), myNewProduct.getName(), "Created product is not present on shop page");
                wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//article[" + (i+1) + "]/div/div/h1/a")));
                driver.findElement(By.xpath("//article[\" + (i+1) + \"]/div/div/h1/a")).click();
                wait.until(ExpectedConditions.presenceOfElementLocated(productViewButton));
                Assert.assertEquals((driver.findElement(productName).getText()), myNewProduct.getName(), "Product name is different.");
                Assert.assertEquals((driver.findElement(productQuantity).getText()), myNewProduct.getQty(), "Product quantity is different.");
                Assert.assertEquals((driver.findElement(productPrice).getText()), myNewProduct.getPrice(), "Product quantity is different.");
                break;
            }

            if (i==(pageSize-1)){
                wait.until(ExpectedConditions.presenceOfElementLocated(nextPageButton));
                driver.findElement(nextPageButton).click();
                try {
                    Thread.sleep(1500);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                i = -1;
                }
            }
        }
    }