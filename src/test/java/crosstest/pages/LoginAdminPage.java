package crosstest.pages;

import crosstest.utils.Properties;
import org.openqa.selenium.By;
import org.openqa.selenium.support.events.EventFiringWebDriver;

public class LoginAdminPage {
    private EventFiringWebDriver driver;
    private By emailInput = By.cssSelector("input[name='email']");
    private By passwordInput = By.id("passwd");
    private By loginButton = By.name("submitLogin");

    public LoginAdminPage (EventFiringWebDriver driver) {this.driver = driver;}

    public void open() {driver.get(Properties.getBaseAdminUrl());}

    public void fillEmailInput(String login) {driver.findElement(emailInput).sendKeys(login);}

    public void fillPasswordInput(String password) {driver.findElement(passwordInput).sendKeys(password);}

    public void clickLoginButton() {driver.findElement(loginButton).click();}
}
