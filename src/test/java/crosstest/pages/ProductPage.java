package crosstest.pages;

import crosstest.model.ProductData;
import crosstest.utils.logging.CustomReporter;
import crosstest.utils.logging.EventHandler;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.events.EventFiringWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.Test;

import static crosstest.utils.logging.CustomReporter.log;


public class ProductPage {
    private EventFiringWebDriver driver;

    //private WebDriverWait wait = new WebDriverWait(driver, 10);
    private By catalogMenuItem = By.id("subtab-AdminCatalog");
    private By productsItem = By.id("subtab-AdminProducts");
    private By addNewProductButton = By.cssSelector("#page-header-desc-configuration-add > i");
    private By productNameField = By.id("form_step1_name_1");
    private By productQuantityField = By.id("form_step1_qty_0_shortcut");
    private By productPriceField = By.id("form_step1_price_shortcut");
    private By activationSwitcherOnTheBottomPanel = By.className("switch-input");
    private By popUpNotificationMessage = By.cssSelector("div[class=\"growl-message\"]");
    private By popUpNotificationCloseButton = By.cssSelector("div[class=\"growl-close\"]");
    private By saveButton = By.cssSelector("input[id=\"submit\"]");
    private By salesScoreElement = By.id("sales_score");


    private By logOutImage = By.cssSelector("div.img-circle");
    private By logOutButton = By.id("header_logout");
    private CustomReporter customReporter;



    public ProductPage (EventFiringWebDriver driver) {this.driver = driver; this.driver.register(new EventHandler());}

@Test
    public void clickOnProductsSubItem() {

        WebDriverWait wait = new WebDriverWait(driver, 10);
        wait.until(ExpectedConditions.elementToBeClickable(catalogMenuItem));
        WebDriverWait waitForSalesScoreElement = new WebDriverWait(driver, 10);
        waitForSalesScoreElement.until(ExpectedConditions.elementToBeClickable(salesScoreElement));
        Actions act = new Actions(driver);
        act.moveToElement(driver.findElement(catalogMenuItem)).perform();
        act.perform();
        act.build();
        act.perform();
        WebDriverWait waitForSubCatalogMenuItem = new WebDriverWait(driver, 10);
        waitForSubCatalogMenuItem.until(ExpectedConditions.elementToBeClickable(productsItem));
        driver.findElement(productsItem).click();
    }

    public void addNewProduct(ProductData myNewProduct) {
        WebDriverWait wait = new WebDriverWait(driver, 10);
        wait.until(ExpectedConditions.elementToBeClickable(addNewProductButton));
        driver.findElement(addNewProductButton).click();
        driver.findElement(productNameField).sendKeys(myNewProduct.getName());
        driver.findElement(productQuantityField).click();
        driver.findElement(productQuantityField).sendKeys(Keys.DELETE, Keys.BACK_SPACE);
        driver.findElement(productQuantityField).sendKeys(myNewProduct.getQty().toString());
        driver.findElement(productPriceField).sendKeys(Keys.CONTROL, "a");
        driver.findElement(productPriceField).sendKeys(Keys.DELETE);
        driver.findElement(productPriceField).sendKeys(myNewProduct.getPrice());
        driver.findElement(activationSwitcherOnTheBottomPanel).click();
        wait.until(ExpectedConditions.presenceOfElementLocated(popUpNotificationMessage));
        driver.findElement(popUpNotificationCloseButton).click();
        driver.findElement(saveButton).click();
        wait.until(ExpectedConditions.presenceOfElementLocated(popUpNotificationMessage));
        driver.findElement(popUpNotificationCloseButton).click();
    }


    public void logOut() {

        driver.navigate().refresh();
        WebDriverWait wait = new WebDriverWait(driver, 10);
        wait.until(ExpectedConditions.presenceOfElementLocated(logOutImage));
        driver.findElement(logOutImage).click();
        wait.until(ExpectedConditions.elementToBeClickable(logOutButton));
        driver.findElement(logOutButton).click();

    }



}
