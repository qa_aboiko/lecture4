package crosstest;

import crosstest.model.ProductData;
import crosstest.pages.LoginAdminPage;
import crosstest.pages.MainShopPage;
import crosstest.pages.ProductPage;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.events.EventFiringWebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 * Contains main script actions that may be used in scripts.
 */
public class GeneralActions {
    //private EventFiringWebDriver driver;
    private WebDriver driver;
    private WebDriverWait wait;
    //
    private ProductData myNewProduct;

    //public GeneralActions(WebDriver driver) {
    public GeneralActions(EventFiringWebDriver driver) {
        this.driver = driver;
        wait = new WebDriverWait(driver, 30);
        //
        myNewProduct = ProductData.generate();
    }

    /**
     * Logs in to Admin Panel.
     * @param login
     * @param password
     */
    public void login(String login, String password) {
        // TODO implement logging in to Admin Panel
        EventFiringWebDriver eventFiringWebDriver = new EventFiringWebDriver(driver);
        LoginAdminPage loginAdminPage = new LoginAdminPage(eventFiringWebDriver);
        loginAdminPage.open();
        loginAdminPage.fillEmailInput(login);
        loginAdminPage.fillPasswordInput(password);
        loginAdminPage.clickLoginButton();
    }


    public void createProduct() {
        EventFiringWebDriver eventFiringWebDriver = new EventFiringWebDriver(driver);
        ProductPage productPage = new ProductPage(eventFiringWebDriver);
        productPage.clickOnProductsSubItem();
        productPage.addNewProduct(myNewProduct);
        productPage.logOut();

    }

    public void productVerification(){
        EventFiringWebDriver eventFiringWebDriver = new EventFiringWebDriver(driver);
        MainShopPage mainShopPage = new MainShopPage(eventFiringWebDriver);
        mainShopPage.open();
        mainShopPage.productDisplayVerification(myNewProduct);
    }

}
