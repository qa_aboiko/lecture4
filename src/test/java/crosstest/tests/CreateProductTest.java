package crosstest.tests;

import crosstest.BaseTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import static crosstest.utils.logging.CustomReporter.log;

public class CreateProductTest extends BaseTest {

    @DataProvider(name = "Authentication")

    public static Object[][] credentials() {

        return new Object[][]{{"webinar.test@gmail.com", "Xcg7299bnSmMuRLp9ITw"}};
    }

    @Test(dataProvider = "Authentication")
    public void createNewProduct(String login, String password) {

        log("Login to admin page");
        actions.login(login, password);
        log("Start of creating product");
        actions.createProduct();
        log("Performing product verification");
        actions.productVerification();
    }

}